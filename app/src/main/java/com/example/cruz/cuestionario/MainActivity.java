package com.example.cruz.cuestionario;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private TextView tvPregunta, tvScore,tvResultado;
    private RadioButton radio1, radio2, radio3;

    private Preguntas preguntas = new Preguntas();
    private String respuesta;
    private int score = 0;
    private int numeroPregunta = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvPregunta = findViewById(R.id.tvPregunta);
        tvResultado= findViewById(R.id.tvResultado);
        tvScore = findViewById(R.id.tvScore);
        radio1 = findViewById(R.id.rbR1P1);
        radio2 = findViewById(R.id.rbR2P1);
        radio3 = findViewById(R.id.rbR3P1);


        actualizarPregunta();

        radio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radio1.getText().toString() == respuesta) {
                    score += 1;
                    actualizarScore(score);
                    verificarPreguntas();
                    Toast.makeText(getApplicationContext(), "Correcto!!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Incoorrecto  :(!!", Toast.LENGTH_LONG).show();
                    verificarPreguntas();
                }
            }
        });
        radio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radio2.getText().toString() == respuesta) {
                    score += 1;
                    actualizarScore(score);
                    verificarPreguntas();
                    Toast.makeText(getApplicationContext(), "Correcto!!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Incoorrecto  :(!!", Toast.LENGTH_LONG).show();
                    verificarPreguntas();
                }
            }
        });
        radio3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radio3.getText().toString() == respuesta) {
                    score += 1;
                    actualizarScore(score);
                    verificarPreguntas();
                    Toast.makeText(getApplicationContext(), "Correcto!!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Incoorrecto  :(!!", Toast.LENGTH_SHORT).show();
                    verificarPreguntas();
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void actualizarPregunta() {
        tvPregunta.setText("Pregunta [" + (numeroPregunta+1) + "]: " + preguntas.getPregunta(numeroPregunta));
        radio1.setText(preguntas.getOpcion1(numeroPregunta));
        radio2.setText(preguntas.getOpcion2(numeroPregunta));
        radio3.setText(preguntas.getOpcion3(numeroPregunta));

        respuesta = preguntas.getRespuestaCorrecta(numeroPregunta);
        numeroPregunta++;
    }

    private void actualizarScore(int puntos) {
        tvScore.setText("" + puntos);
    }

    private void verificarPreguntas() {
        if (numeroPregunta < preguntas.preguntas.length) {
            actualizarPregunta();
            radio1.setChecked(false);
            radio2.setChecked(false);
            radio3.setChecked(false);

        } else {
            Toast.makeText(this, "Felicitaciones, conseguiste " + score + "puntos", Toast.LENGTH_LONG).show();
            radio1.setEnabled(false);
            radio2.setEnabled(false);
            radio3.setEnabled(false);
            tvResultado.setText("Haz concluido con el Quiz, Felicidades!!");
        }
    }
}
