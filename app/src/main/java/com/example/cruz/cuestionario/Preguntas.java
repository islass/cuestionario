package com.example.cruz.cuestionario;

/**
 * Created by Laura on 16/02/2019.
 */

public class Preguntas {
    public String preguntas[] = {
            "¿Cual es el rio mas largo?",
            "¿Quien es el padre de la ciencia?",
            "¿Cuantos huesos tiene el cuerpo humano?",
            "¿Cuando acabo la segunda guerra mundial?",
            "¿Donde e escuentra la torre de pisa?",
            "¿Que son los humanos?",
            "¿Cual es el oceano mas grande?",
            "¿En que año llego Cristobal Colon a Amerfica?",
            "¿Como se llama el actual presidente de Estados unidos?",
            "¿Pais mas grande del mundo?",
            "¿Donde se encuentra la famosa torre Eiffel?",
            "¿Que practicaba Michael Jordan?",
            "¿En que año comenzo la 2da guerra mundial?",
            "¿Cual es el tercer planeta en el sistema solar?",
            "¿Cual es el pais mas poblado de la tierra?",
            "¿Cuantas patas tiene una araña?",
            "¿Animal mas rapido del mundo?",
            "¿Cual es la ciudad de los rascacielos?",
            "¿Cual fue el primer metal que empleo el hombre?",
            "¿Quien gano el mundial 2014?",
            "¿Quien traiciono a Jesus?"

    };
    private String respuestas[][] = {
            {"Nilo", "Amazonas", "Bravo"},
            {"Newton","Galileo","Bill gates"},
            {"206","250","180"},
            {"1950","1945","1997"},
            {"Mexico","Paris","Italia"},
            {"Omnivoros","Hervivoros","Carnivoros"},
            {"Pacifico","Indico","Atlantico"},
            {"1500","1453","1492"},
            {"Donal Trumnp","Barack Obama","Hillary clinton"},
            {"Estados Unidos","Rusia","Brasil"},
            {"Paris","Venecia","Firenze"},
            {"Futbol","Baloncesto","Tennnis"},
            {"1900","1939","1915"},
            {"Jupiter","Marte","Tierra"},
            {"China","India","Indonesia"},
            {"12","6","8"},
            {"Guepardo","Chita","Jaguar"},
            {"CDMX","Nueva York","Las vegas"},
            {"Aluminio","Cobre","Bronce"},
            {"España","Francia","Brasil"},
            {"Judas","Pedro","Simon"}
    };
    private String respuestasCorrestas[] = {
            "Amazonas",
            "Galileo",
            "206",
            "1945",
            "Italia",
            "Omnivoros",
            "Pacifico",
            "1492",
            "Donal Trumnp",
            "Rusia",
            "Paris",
            "Baloncesto",
            "1939",
            "Tierra",
            "China",
            "8",
            "Guepardo",
            "Nueva York",
            "Cobre",
            "Brasil",
            "Judas"
    };

    public String getPregunta(int a){
        String pregunta= preguntas[a];
        return pregunta;
    }
    public String getOpcion1(int a){
        String opcion1=respuestas[a][0];
        return opcion1;
    }

    public String getOpcion2(int a){
        String opcion2=respuestas[a][1];
        return opcion2;
    }

    public String getOpcion3(int a){
        String opcion3=respuestas[a][2];
        return opcion3;
    }
    public String getRespuestaCorrecta(int a){
        String respuesta= respuestasCorrestas[a];
        return  respuesta;
    }

}